package com.ie.khanebedoosh.domain;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Manager {

    public Manager() {
    }



    private static List<Individual> individuals;
    private static List<RealState> realStates;
    static{
        individuals = new ArrayList<Individual>();
        realStates = new ArrayList<RealState>();
        individuals.add(new Individual("بهنام همایون", null,
                0, "behnamhomayoon", null));
        realStates.add(new RealState("KhaneBeDoosh","http://acm.ut.ac.ir/khaneBeDoosh/house"));
    }

    public static List<Individual> getIndividuals() {
        return individuals;
    }

    public static List<RealState> getRealStates() {
        return realStates;
    }

    public static JSONObject getAllHouses(RealState realState) throws IOException {
        Logger logger = Logger.getLogger("com.ie");
        logger.setLevel(Level.INFO);
        return realState.getAllHouses();
    }
}
