package com.ie.khanebedoosh.domain;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.ie.khanebedoosh.domain.House.createHouseFromJSON;

public class RealState extends User {
    private String url;

    public RealState(String name, String url) {
        super(name);
        this.url = url;
    }

    public JSONObject getAllHouses() throws IOException {
        Logger logger = Logger.getLogger("com.ie");
        logger.setLevel(Level.INFO);
        URL obj = new URL(this.url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod("GET");
        int responseCode = con.getResponseCode();
        //        logger.warning("\nSending 'GET' request to URL : " + url);
//        logger.warning("Response Code : " + responseCode);
        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream(), "UTF-8"));
        String inputLine;
        StringBuffer response = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
        //        logger.warning(response.toString());
        JSONObject myResponse = new JSONObject(response.toString());
        return myResponse;
    }

    public House getHouseByID(String id) throws IOException {
        Logger logger = Logger.getLogger("com.ie");
        logger.setLevel(Level.INFO);
            String url = this.url+ "/" + id;
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod("GET");
//            int responseCode = con.getResponseCode();
//            logger.warning("\nSending 'GET' request to URL : " + url);
//            logger.warning("Response Code : " + responseCode);
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream(), "UTF-8"));
            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            JSONObject myResponse = new JSONObject(response.toString());
            House house = createHouseFromJSON(myResponse.getJSONObject("data"));
//            logger.warning(house.toJSONString());
            return house;
    }

    public ArrayList<String> getAllHouseIDs() throws IOException {
        ArrayList<String> IDs = new ArrayList<String>();
        JSONObject housesJSON = this.getAllHouses();
        JSONArray jsonArray = housesJSON.getJSONArray("data");
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject houseJSON = jsonArray.getJSONObject(i);
            IDs.add(houseJSON.getString("id"));
        }
        return IDs;
    }

    public String getUrl() {
        return url;
    }
}
