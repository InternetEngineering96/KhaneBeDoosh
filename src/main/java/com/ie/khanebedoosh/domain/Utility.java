package com.ie.khanebedoosh.domain;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Utility {

    public static void addHouseFromRequest(Individual individual, HttpServletRequest request){
        Logger logger = Logger.getLogger("com.ie");
        logger.setLevel(Level.INFO);
            // logger.warning("increased value is:");
            int dealType;
            int basePrice = 0;
            int rentPrice = 0;
            int sellPrice = 0;
            if (request.getParameterMap().containsKey("buildingType") && request.getParameterMap().containsKey("area")
                    && request.getParameterMap().containsKey("price") && request.getParameterMap().containsKey("address")
                    && request.getParameterMap().containsKey("description") && request.getParameterMap().containsKey("dealType")) {
                if (request.getParameter("buildingType").equals("") || request.getParameter("area").equals("")
                        || request.getParameter("price").equals("") || request.getParameter("address").equals("")
                        || request.getParameter("description").equals("") || request.getParameter("dealType").equals(""))
                    throw new IllegalArgumentException();
            } else throw new IllegalArgumentException();
            if (request.getParameter("dealType").equals("0")) {
                dealType = 0;
                sellPrice = Integer.parseInt(request.getParameter("price"));
            } else if (request.getParameter("dealType").equals("1")) {
                dealType = 1;
                rentPrice = Integer.parseInt(request.getParameter("price"));
            } else {
                throw new IllegalArgumentException();
            }
            String address = request.getParameter("address");
            String description = request.getParameter("description");
            String phone = request.getParameter("phone");
//            if (address.equals(""))
//                address = null;
//            if(description.equals(""))
//                description = null;
            if (basePrice < 0 || rentPrice < 0 || sellPrice < 0)
                throw new IllegalArgumentException();
            if(phone.equals(""))
                throw new IllegalArgumentException();
            individual.addHouse(UUID.randomUUID().toString(), request.getParameter("buildingType"),
                    address, null, phone,
                    description, null,
                    Integer.parseInt(request.getParameter("area")),
                    basePrice, rentPrice, sellPrice, dealType);
//            ArrayList<House> houses = this.getHouses();
//            for (House house : houses) {
//                logger.warning("House created: "+ house.toJSONString());
//            }
    }

    public static House findHouseByID(String id, Individual individual, RealState realState) throws IOException {
//        RealState realState = Manager.getRealStates().get(0);
        House house = individual.getHousebyID(id);
        if(house == null){
            house = realState.getHouseByID(id);
        }
        return house;
    }

    public static void handlePhoneRequest(HttpServletRequest request, Individual individual){
        String houseID = request.getParameter("id");
        request.setAttribute("Searched","");
        if (individual.getBalance() >= 1000 && !individual.isPhoneNumBought(houseID)) {
            request.setAttribute("successfulDecrease", "");
            individual.addBoughtHouseID(houseID);
            individual.decreaseBalance();
        }
        else if (individual.getBalance() < 1000){
            request.setAttribute("fail", "");
        }
        else if(individual.isPhoneNumBought(houseID))
        {
            request.setAttribute("successfulDecrease", "");
        }
    }
}
