package com.ie.khanebedoosh.domain;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.IOException;

public class Bank {
    public static boolean sendPayRequestAndGetResponse(int value) throws IOException {
        HttpClient httpClient = HttpClientBuilder.create().build();
        HttpPost request = new HttpPost("http://acm.ut.ac.ir/ieBank/pay");
        String bodyParam = "{\"userId\":\"123\",\"value\":\"" + value + "\"}";
        StringEntity params = new StringEntity(bodyParam);
        request.addHeader("Content-type", "application/json");
        //the apiKey is for user with name 810193368-810193361
        request.addHeader("apiKey", "1f897630-12f5-11e8-87b4-496f79ef1988");
        request.setEntity(params);
        HttpResponse response = httpClient.execute(request);
        if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK)
            return true;
        else if (response.getStatusLine().getStatusCode() == HttpStatus.SC_INTERNAL_SERVER_ERROR)
            return false;
        else
            throw new HttpResponseException(response.getStatusLine().getStatusCode(), "Unexpected " + response.getStatusLine().getStatusCode());
    }
}
