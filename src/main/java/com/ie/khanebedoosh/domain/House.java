package com.ie.khanebedoosh.domain;
import org.json.JSONObject;

public class House {
    private int area, basePrice, rentPrice, sellPrice;
    private String id, buildingType, address, imageURL, phone, description, expireTime;

//    private DealType dealType;
    private int dealType;
    public House(String id, String buildingType, String address, String imageURL, String phone,
                 String description, String expireTime, int area, int basePrice, int rentPrice,
                 int sellPrice, int dealType) {
        this.id = id;
        this.buildingType = buildingType;
        this.address = address;
        this.imageURL = imageURL;
        this.phone = phone;
        this.description = description;
        this.expireTime = expireTime;
        this.area = area;
        this.basePrice = basePrice;
        this.rentPrice = rentPrice;
        this.sellPrice = sellPrice;
        this.dealType = dealType;
    }

    public String toJSONString(){
        JSONObject jsonObject = new JSONObject(this);
        return jsonObject.toString();
    }

    public JSONObject toJSON(){
        return new JSONObject(this);
    }

    public static House createHouseFromJSON(JSONObject house){
        String id = house.getString("id");
        int area = house.getInt("area");
        String address = house.getString("address");
        int dealType = house.getInt("dealType");
        String phone = house.getString("phone");
        String description = house.getString("description");
        String expireTime = house.getString("expireTime");
        String imageURL = house.getString("imageURL");
        int rentPrice = 0;
        int sellPrice = 0;
        int basePrice = 0;

        if (dealType == 1 )
            rentPrice = house.getJSONObject("price").getInt("rentPrice");
        else if (dealType == 0)
            sellPrice = house.getJSONObject("price").getInt("sellPrice");

        String buildingType = new String();
        if(house.getString("buildingType").equals("ویلایی"))
            buildingType += "villa";
        else if(house.getString("buildingType").equals("آپارتمان"))
            buildingType+= "apartment";
        House createdHouse = new House(id,buildingType,address,imageURL,phone,description,expireTime,
                area,basePrice,rentPrice,sellPrice,dealType);
        return createdHouse;
    }

    //public enum DealType {rent, sale}

    public String getDealTypeString(){
        if(this.dealType==0)
            return new String("فروش");
        else
            return new String("رهن و اجاره");
    }

    public int getArea() {
        return area;
    }

    public int getBasePrice() {
        return basePrice;
    }

    public int getRentPrice() {
        return rentPrice;
    }

    public int getSellPrice() {
        return sellPrice;
    }

    public String getId() {
        return id;
    }

    public String getBuildingType() {
        return buildingType;
    }

    public String getPersianBuildingType() {
        if(this.getBuildingType().equals("apartment"))
            return "آپارتمان";
        else if (this.getBuildingType().equals("villa"))
            return "ویلایی";
        return null;
    }

    public String getAddress() {
        return address;
    }

    public String getImageURL() {
        return imageURL;
    }

    public String getPhone() {
        return phone;
    }

    public String getDescription() {
        return description;
    }

    public String getExpireTime() {
        return expireTime;
    }

    public int getDealType() {
        return dealType;
    }
}
