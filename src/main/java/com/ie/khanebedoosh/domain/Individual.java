package com.ie.khanebedoosh.domain;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

public class Individual extends User {

    private static String phone;
    private static int balance;
    private static String username;
    private static String password;
    public static ArrayList<String> boughtHouseIDs = new ArrayList<String>();
    private static ArrayList<House> houses = new ArrayList<House>();

    public Individual(String name, String phone, int balance, String username, String password) {
        super(name);
        this.phone = phone;
        this.balance = balance;
        this.username = username;
        this.password = password;
    }

    public JSONObject toJSON(){
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("name",this.getName());
        jsonObject.put("balance",this.getBalance());
        return jsonObject;
    }

    public void addHouse(String id, String buildingType, String address, String imageURL, String phone,
                         String description, String expireTime, int area, int basePrice, int rentPrice,
                         int sellPrice, int dealType) {
        this.houses.add(new House(id, buildingType, address, imageURL, phone, description, expireTime, area,
                basePrice, rentPrice, sellPrice, dealType));
    }

    public boolean increaseBalance(int value) throws IOException {
        boolean status = Bank.sendPayRequestAndGetResponse(value);
        if (status)
            balance += value;
        return status;
    }
    public void decreaseBalance(){
        balance -= 1000;
    }

    public ArrayList<String> getBoughtHouseIDs() {
        return boughtHouseIDs;
    }
    public void addBoughtHouseID(String id){
        boughtHouseIDs.add(id);
    }

    public boolean isPhoneNumBought(String id) {
        for (int i = 0; i < boughtHouseIDs.size(); i++) {
            if (boughtHouseIDs.get(i).equals(id))
                return true;
        }
        return false;
    }
    public ArrayList<House> getHouses() {
        return houses;
    }

    public static House getHousebyID(String id){
        for(House house: houses){
            if(house.getId().equals(id))
                return house;
        }
        return null;
    }

    public ArrayList<String> getAllHouseIDs() {
        ArrayList<String> IDs = new ArrayList<String>();
        for(House house : houses){
            IDs.add(house.getId());
        }
        return IDs;
    }

    public String getPhone() {
        return phone;
    }

    public int getBalance() {
        return balance;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

}
