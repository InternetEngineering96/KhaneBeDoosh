package com.ie.khanebedoosh.domain;

import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONArray;
import org.json.JSONObject;


public class Search {
    private Integer minArea;
    private String buildingType;
    private Integer dealType;
    private Integer maxRentPrice;
    private Integer maxSellPrice;

    public Search(String minArea, String buildingType, String dealType, String maxPrice) {
        this.minArea = null;
        this.buildingType = null;
        this.dealType = null;
        this.maxRentPrice = null;
        this.maxSellPrice = null;
        validateSearchQuery(minArea, buildingType, dealType, maxPrice);
    }

    private void validateSearchQuery(String minArea, String buildingType, String dealType, String maxPrice) {
        if (!minArea.equals("")) {
            this.minArea = Integer.parseInt(minArea);
            if (this.minArea < 0)
                throw new IllegalArgumentException();
        }
        if (!buildingType.equals(""))
            if (buildingType.equals("villa") || buildingType.equals("apartment"))
                this.buildingType = buildingType;
            else
                throw new IllegalArgumentException();
        if (!dealType.equals("")) {
            this.dealType = Integer.parseInt(dealType);
            if (!(this.dealType == 1 || this.dealType == 0))
                throw new IllegalArgumentException();
            if (!maxPrice.equals("")) {
                if (Integer.parseInt(maxPrice) < 0)
                    throw new IllegalArgumentException();
                if (dealType.equals("1")) {
                    maxRentPrice = Integer.parseInt(maxPrice);
                } else if (dealType.equals("0")) {
                    maxSellPrice = Integer.parseInt(maxPrice);
                } else
                    throw new IllegalArgumentException();
            }
        } else if (!maxPrice.equals("")) { //when dealType is not specified but maxPrice is!
            if (Integer.parseInt(maxPrice) < 0)
                throw new IllegalArgumentException();
            maxRentPrice = Integer.parseInt(maxPrice);
            maxSellPrice = Integer.parseInt(maxPrice);
        }
    }

    private boolean evaluateHouseJSON(JSONObject json) {
        boolean isAreaOK = isAreaOK(json);
        boolean isDealTypeOK = isDealTypeOK(json);
        boolean isBuildingTypeOK = isBuildingTypeOK(json);
        boolean isPriceOK = isPriceOK(json, isDealTypeOK);
        return isAreaOK && isBuildingTypeOK && isDealTypeOK && isPriceOK;
    }

    private boolean isPriceOK(JSONObject json, boolean isDealTypeOK) {
        if (this.dealType != null && isDealTypeOK) {
            if (this.dealType == 1)
                if ((this.maxRentPrice != null &&
                        json.getJSONObject("price").getInt("rentPrice") <= this.maxRentPrice) ||
                        this.maxRentPrice == null)
                    return true;
            if (this.dealType == 0)
                if ((this.maxSellPrice != null &&
                        json.getJSONObject("price").getInt("sellPrice") <= this.maxSellPrice) ||
                        this.maxSellPrice == null)
                    return true;
        }
        if (this.dealType == null) {
            if ((this.maxRentPrice != null && json.getInt("dealType") == 1 &&
                    json.getJSONObject("price").getInt("rentPrice") <= this.maxRentPrice) ||
                    this.maxRentPrice == null)
                return true;
            if ((this.maxSellPrice != null && json.getInt("dealType") == 0 &&
                    json.getJSONObject("price").getInt("sellPrice") <= this.maxSellPrice) ||
                    this.maxSellPrice == null)
                return true;
        }
        return false;
    }

    private boolean isBuildingTypeOK(JSONObject json) {
        return (this.buildingType == null ||
                ((this.buildingType.equals("villa") && json.getString("buildingType").equals("ویلایی")) ||
                        (this.buildingType.equals("apartment") &&
                                json.getString("buildingType").equals("آپارتمان"))));
    }

    private boolean isDealTypeOK(JSONObject json) {
        return ((this.dealType != null && json.getInt("dealType") == this.dealType) ||
                this.dealType == null);
    }

    private boolean isAreaOK(JSONObject json) {
        return ((this.minArea != null && json.getInt("area") >= this.minArea) || this.minArea == null);
    }

    private boolean evaluateHouseObject(House house) {
        boolean isAreaOK = isAreaOK(house);
        boolean isDealTypeOK = isDealTypeOK(house);
        boolean isBuildingTypeOK = isBuildingTypeOK(house);
        boolean isPriceOK = isPriceOK(house, isDealTypeOK);
        return isAreaOK && isBuildingTypeOK && isDealTypeOK && isPriceOK;
    }

    private boolean isPriceOK(House house, boolean isDealTypeOK) {
        if (this.dealType != null && isDealTypeOK) {
            if (this.dealType == 1)
                if ((this.maxRentPrice != null && house.getRentPrice() <= this.maxRentPrice)
                        || this.maxRentPrice == null)
                    return true;
            if (this.dealType == 0)
                if ((this.maxSellPrice != null && house.getSellPrice() <= this.maxSellPrice)
                        || this.maxSellPrice == null)
                    return true;
        }
        if (this.dealType == null) {
            if ((this.maxRentPrice != null && house.getDealType() == 1 &&
                    house.getRentPrice() <= this.maxRentPrice) ||
                    this.maxRentPrice == null)
                return true;
            if ((this.maxSellPrice != null && house.getDealType() == 0 &&
                    house.getSellPrice() <= this.maxSellPrice) ||
                    this.maxSellPrice == null)
                return true;
        }
        return false;
    }

    private boolean isBuildingTypeOK(House house) {
        return (this.buildingType == null ||
                this.buildingType.equals(house.getBuildingType()));
    }

    private boolean isDealTypeOK(House house) {
        return ((this.dealType != null && house.getDealType() == this.dealType) ||
                this.dealType == null);
    }

    private boolean isAreaOK(House house) {
        return ((this.minArea != null && house.getArea() >= this.minArea) || this.minArea == null);
    }

    public static ArrayList<String> findRealStateHouseIDs(Search query, RealState realState) throws IOException {
        ArrayList<String> searchResults = new ArrayList<String>();
        JSONObject housesJSON = Manager.getAllHouses(realState);
        JSONArray jsonArray = housesJSON.getJSONArray("data");
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject houseJSON = jsonArray.getJSONObject(i);
            if (query.evaluateHouseJSON(houseJSON)) {
                searchResults.add(houseJSON.getString("id"));
            }
        }
        return searchResults;
    }

    public static ArrayList<House> getRequestedHousesFromAllUsers(Search query) throws IOException {
        ArrayList<House> requestedHouses = new ArrayList<House>();
        RealState realState = Manager.getRealStates().get(0);
        ArrayList<String> realStateHouseIDs = findRealStateHouseIDs(query, realState);
        for (Individual individual : Manager.getIndividuals()) {
            searchIndividualsHouses(individual, query, requestedHouses);
        }
        searchRealStateHouses(realStateHouseIDs, realState, requestedHouses);
        return requestedHouses;
    }

    private static void searchRealStateHouses(ArrayList<String> IDs, RealState realState, ArrayList<House> requestedHouses) throws IOException {
        Logger logger = Logger.getLogger("com.ie");
        logger.setLevel(Level.INFO);
        for (String id : IDs) {
            House house = realState.getHouseByID(id);
            requestedHouses.add(house);
        }
    }

    private static void searchIndividualsHouses(Individual user, Search query, ArrayList<House> requestedHouses) {
        Logger logger = Logger.getLogger("com.ie");
        logger.setLevel(Level.INFO);
        for (House house : user.getHouses()) {
            if (query.evaluateHouseObject(house)) {
                requestedHouses.add(house);
//                logger.warning(house.toJSONString());
            }
        }
    }

//    public static JSONObject buildJSONFromSearchRequest(HttpServletRequest request) {
//        Logger logger = Logger.getLogger("com.ie");
//        logger.setLevel(Level.INFO);
//        JSONObject json = new JSONObject();
//        json.put("area", request.getParameter("area"));
//        json.put("buildingType", request.getParameter("buildingType"));
//        json.put("dealType", request.getParameter("dealType"));
//        JSONObject item = new JSONObject();
//        if (request.getParameter("dealType").equals("1")) {
//            item.put("rentPrice", request.getParameter("maxPrice"));
//        } else if (request.getParameter("dealType").equals("0")) {
//            item.put("sellPrice", request.getParameter("maxPrice"));
//        } else
//            throw new IllegalArgumentException();
//        json.put("price", item);
//        logger.warning(json.toString());
//        return json;
//    }

}
