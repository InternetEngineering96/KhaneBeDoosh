package com.ie.khanebedoosh.controller;

import com.ie.khanebedoosh.domain.*;
import org.json.JSONObject;

import java.util.logging.Logger;
import java.util.logging.Level;
import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;

@WebServlet("/users/*")
public class userController extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Logger logger = Logger.getLogger("com.ie");
        logger.setLevel(Level.INFO);
        JSONObject json = new JSONObject();
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
//        response.addHeader("Access-Control-Allow-Origin", "*");
//        response.addHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE, HEAD");
//        response.addHeader("Access-Control-Allow-Headers", "X-PINGOTHER, Origin, X-Requested-With, Content-Type, Accept");
//        response.addHeader("Access-Control-Max-Age", "1728000");
        Individual individual = Manager.getIndividuals().get(0);
        try {
            if (request.getParameterMap().containsKey("username")) {
                if (request.getParameter("username").equals(individual.getUsername())) {
                    json.put("individual", individual.toJSON());
                } else throw new IllegalArgumentException();
                if(request.getParameterMap().containsKey("houseID")){
                    json.put("hasPayed", individual.isPhoneNumBought(request.getParameter("houseID")));
                }
                response.setStatus(HttpServletResponse.SC_OK);
            }
            else throw new IllegalArgumentException();
        } catch(IllegalArgumentException ex){
                json.put("msg", "Illegal Argument in your request!");
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            }
            response.getWriter().write(json.toString());
        }
    }
