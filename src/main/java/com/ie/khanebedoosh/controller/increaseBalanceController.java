package com.ie.khanebedoosh.controller;
import com.ie.khanebedoosh.domain.*;
import org.json.JSONObject;

import java.util.logging.Logger;
import java.util.logging.Level;
import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;

@WebServlet("/balance")
public class increaseBalanceController extends HttpServlet {
        protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            Logger logger = Logger.getLogger("com.ie");
            logger.setLevel(Level.INFO);
            JSONObject json = new JSONObject();
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            Individual individual = Manager.getIndividuals().get(0);
            try {
                int increasedValue = Integer.parseInt(request.getParameter("balance"));
                boolean status = individual.increaseBalance(increasedValue);
                if (status) {
                    json.put("msg", "Balance Increased by " + increasedValue + " successfully!");
                    response.setStatus(HttpServletResponse.SC_OK);
                } else {
                    json.put("msg", "Failed to Increased by " + increasedValue + ", internal server error");
                    response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                }
            } catch (Exception ex){
                json.put("msg", "Error in increasing balance! Try Again!");
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                logger.warning(ex.getMessage() + " Error in increasing balance!");
            }
            response.getWriter().write(json.toString());
        }
}
