<%@ page import="com.ie.khanebedoosh.*" contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ie.khanebedoosh.domain.Manager" %>
<%@ page import="com.ie.khanebedoosh.domain.Individual" %>
<%@ page import="java.util.logging.Logger" %>
<%@ page import="java.util.logging.Level" %>

<html>
<head><title>خانه‌به‌دوش</title></head>
<body>
<%
    Individual individual = Manager.getIndividuals().get(0);
%>
<p align="right"> نام کاربری: <%=individual.getName()%>
</p>
<p align="right"> اعتبار شما: <%=individual.getBalance()%>
</p>
<br>
<%
    if (request.getAttribute("msg") != null) {
%>
<h3><%= request.getAttribute("msg") %>
</h3>
<%
    }
%>
<br>
<form action="houses" method="GET" align="center">
    <input type="number" name="minArea" placeholder="حداقل متراژ" align="right"/><br>
    <select name="buildingType">
        <option value="villa" selected>ویلایی</option>
        <option value="apartment" selected>آپارتمان</option>
    </select><br/>
    <input type="number" name="dealType" min="0" max="1" placeholder="نوع قرارداد (خرید/اجاره)" align="right"/><br>
    <%--<select name="dealType">--%>
    <%--<option value="sell" selected>خرید</option>--%>
    <%--<option value="rent" selected>اجاره</option>--%>
    <%--</select>--%>
    <input type="number" name="maxPrice" placeholder="حداکثر قیمت" align="right"/><br>
    <input type="submit" value="جست‌وجو" align="right"/>
</form>

<form action="houses" method="POST" align="center" accept-charset="utf-8">
    <%--<input type="text" name="buildingType" placeholder="نوع ساختمان (ویلایی/آپارتمان)" align="right"/><br>--%>
    <select name="buildingType">
        <option value="villa" selected>ویلایی</option>
        <option value="apartment" selected>آپارتمان</option>
    </select>
    <br/>
    <input type="number" name="dealType" min="0" max="1" placeholder="نوع قرارداد (خرید/اجاره)"
           align="right" /><br>
    <%--<select name="dealType">--%>
    <%--<option value="sell" selected>خرید</option>--%>
    <%--<option value="rent" selected>اجاره</option>--%>
    <%--</select>--%>
    <input type="number" name="area" placeholder="متراژ" align="right" /><br>
    <input type="number" name="price" placeholder="قیمت فروش/اجاره" align="right"/><br>
    <input type="text" name="address" placeholder="آدرس" align="right" /><br>
    <input type="text" name="phone" placeholder="شماره‌ی تلفن" align="right"/><br>
    <input type="text" name="description" placeholder="توضیحات" align="right"/><br>
    <input type="submit" value="اضافه‌کردن خانه‌ی جدید" align="right"/>

</form>

<form action="balance" method="POST" align="center">
    <input type="number" min="0" name="balance" placeholder="اعتبار" align="right"/><br>
    <input type="submit" value="افزایش اعتبار" align="right"/>
</form>

</body>
</html>
